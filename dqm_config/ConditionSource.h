/*
 * ConditionSource.h
 *
 *  Created on: Nov 12, 2013
 *      Author: kolos
 */

#ifndef _DQM_CONFIG_CONDITION_SOURCE_H_
#define _DQM_CONFIG_CONDITION_SOURCE_H_

#include <mutex>

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/member.hpp>

#include <is/infowatcher.h>
#include <is/infodynany.h>

namespace dqm_config
{
    struct Condition
    {
	Condition(const std::string & n, const std::string & a, const std::string & d);

	void setValue(const std::string & v) const;
	void setValue(double v) const;

	const std::string 	m_name;
	const std::string 	m_attribute_name;
	mutable std::string	m_string_value;
	mutable double		m_double_value = 0.0;
	mutable bool		m_is_numeric = true;
    };

    using namespace boost::multi_index;

    typedef boost::multi_index_container<
	Condition,
	indexed_by<
	    ordered_unique<
		member<Condition, const std::string, &Condition::m_name>
	    >
    	>
    > Conditions;

    class ConditionSource
    {
    public:
	ConditionSource(const IPCPartition & p, const std::string & object_name);

	void addCondition(const std::string & condition_name,
	    const std::string & attribute_name, const std::string & default_value);

	bool getConditionValue(const std::string & condition_name, double & value) const;

	bool getConditionValue(const std::string & condition_name, std::string & value) const;

    private:
	void valueReceived(const std::string & name, const ISInfoDynAny & info);

    private:
	mutable std::mutex m_mutex;
	Conditions m_conditions;
	ISInfoWatcher<ISInfoDynAny> m_info_watcher;
    };
}

#endif /* _DQM_CONFIG_CONDITION_H_ */
