/*
 * AlgorithmConfigFactory.h
 *
 *  Created on: Nov 13, 2013
 *      Author: kolos
 */

#ifndef _DQM_CONFIG_ALGORITHMCONFIG_FACTORY_H_
#define _DQM_CONFIG_ALGORITHMCONFIG_FACTORY_H_

#include <is/infowatcher.h>
#include <is/infodynany.h>

#include <dqm_config/ConditionsManager.h>
#include <dqm_config/ReferenceManager.h>
#include <dqm_config/RunStateWatcher.h>
#include <dqm_config/RunType.h>
#include <dqm_config/is/DQParameter.h>
#include <dqm_config/dal/DQParameter.h>

namespace dqm_config
{
    class AlgorithmConfig;

    class AlgorithmConfigFactory
    {
    public:
	AlgorithmConfigFactory(const std::string & partition_name);

	std::shared_ptr<dqm_config::AlgorithmConfig> createAlgorithmConfig(const dal::DQParameter & c);

	ConditionsManager & getConditionsManager() { return m_cman; }

	ReferenceManager & getReferenceManager() { return m_refman; }

	RunStateWatcher & getRunStateWatcher() { return m_rs_watcher; }

	void setRunType(RunType t) {
	    m_run_type = t;
	    m_refman.setRunType(m_run_type);
	}

        RunType getRunType() const {
            return m_run_type;
        }

        void updateRunParameters();

    private:
	AlgorithmConfig * newAlgorithmConfig(const dal::DQParameter & c);

	void valueReceived(const std::string & name, const is::DQParameter & config);

    private:
	typedef std::unordered_map<std::string, std::shared_ptr<AlgorithmConfig>> Products;
	typedef std::unordered_map<std::string, is::DQParameter> DynamicData;
	typedef ISInfoWatcher<is::DQParameter> DynamicDataSource;

	mutable std::mutex	m_mutex;
	std::string             m_partition_name;
	ConditionsManager	m_cman;
	ReferenceManager	m_refman;
	RunStateWatcher		m_rs_watcher;
	Products		m_products;
	DynamicData		m_dynamic_data;
	DynamicDataSource	m_dynamic_data_source;
        RunType                 m_run_type = RunType::Default;
    };
}

#endif /* _DQM_CONFIG_ALGORITHMCONFIG_FACTORY_H_ */
