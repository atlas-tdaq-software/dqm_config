/*
 * ConditionsManager.h
 *
 *  Created on: Nov 12, 2013
 *      Author: kolos
 */

#ifndef _DQM_CONFIG_CONDITIONSMANAGER_H_
#define _DQM_CONFIG_CONDITIONSMANAGER_H_

#include <string>
#include <unordered_map>

#include <dqm_config/ConditionSource.h>

namespace dqm_config
{
    class ConditionsManager
    {
    public:
	ConditionsManager(const std::string & partition_name);

	void addCondition(const std::string & name, const std::string & source, const std::string & default_value);

	bool getConditionValue(const std::string & name, std::string & value) const;

	bool getConditionValue(const std::string & name, double & value) const;

    private:
	typedef std::unordered_map<std::string, std::shared_ptr<ConditionSource>> Conditions;

	mutable std::mutex	m_mutex;
	IPCPartition		m_partition;
	Conditions		m_conditions_sources;
	Conditions		m_conditions;
    };
}

#endif /* _DQM_CONFIG_CONDITIONSMANAGER_H_ */
