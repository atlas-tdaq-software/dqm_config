#ifndef _DQM_CONFIG_EXCEPTIONS_H_
#define _DQM_CONFIG_EXCEPTIONS_H_

/*! \file exceptions.h Declares DQM exceptions.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <ers/ers.h>

namespace dqm_config
{
    inline void fatal( const ers::Issue & issue )
    {
    	ers::fatal( issue );
	exit( 13 );
    }
}

/*! \class dqm_config::Exception
 *  This is a base class for all DQM exceptions.
 */
ERS_DECLARE_ISSUE( dqm_config, Exception, ERS_EMPTY, ERS_EMPTY )

/*! \class dqm_config::BadConfig
 *  This issue is reported if configuration infomation given to DQM Parameter is invalid.
 */
ERS_DECLARE_ISSUE_BASE(	    dqm_config,
			    BadConfig,
			    dqm_config::Exception,
			    "Incorrect configuration (" << reason << ") was provided for the '" << name << "' object",
			    ERS_EMPTY,
			    ((std::string)name )
			    ((std::string)reason )
		    )
/*! \class dqm_config::CantOpenRootFile
 *  This issue is reported if the root file can't be open
 */
ERS_DECLARE_ISSUE_BASE(	    dqm_config,
			    CantOpenRootFile,
			    dqm_config::Exception,
			    "Cannot open root file: (" << name << ") ",
			    ERS_EMPTY,
			    ((std::string)name )
		    )

/*! \class dqm_config::CantGetHistogram
 *  This issue is reported if the histogram can not be retrieved from the root
 file
 */
ERS_DECLARE_ISSUE_BASE(	    dqm_config,
			    CantReadReference,
			    dqm_config::Exception,
			    "Cannot read reference object '" << object_name << "' from the '" << file_name << "' ROOT file",
			    ERS_EMPTY,
			    ((std::string)object_name)
			    ((std::string)file_name)
		    )
		    
ERS_DECLARE_ISSUE_BASE(	dqm_config, 
			ShapeNotDefined, 
			dqm_config::Exception,
			classname<<": '" << name << "' does not have a shape defined in the database", 
			ERS_EMPTY,
			((std::string)classname)
			((std::string)name)
		      )    
ERS_DECLARE_ISSUE_BASE( dqm_config, 
                        ShapeNotFound, 
			dqm_config::Exception,
			"Shape '" << shape << "' is not implemented in the display", 
			ERS_EMPTY,
			((std::string)shape)
		      )	
ERS_DECLARE_ISSUE_BASE( dqm_config, 
                        LayoutNotFound, 
			dqm_config::Exception,
			"Layout '" << name << "' is not implemented in the display",
			ERS_EMPTY, 
			((std::string)name)
		      )
ERS_DECLARE_ISSUE_BASE( dqm_config, 
			LayoutNotDefined, 
			dqm_config::Exception,
			"DQRegion '" << name << "' does not have a layout defined in the database", 
			ERS_EMPTY,
			((std::string)name)
		      )	
		    
#endif // _DQM_CONFIG_EXCEPTIONS_H_
