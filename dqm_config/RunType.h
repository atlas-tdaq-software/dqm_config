/*
 * RunType.h
 *
 *  Created on: Oct 11, 2024
 *      Author: kolos
 */

#ifndef DQM_CONFIG_RUNTYPE_H_
#define DQM_CONFIG_RUNTYPE_H_

namespace dqm_config {
    enum RunType {Default = 0, PP, HI, Cosmic};

    inline std::string toString(RunType t) {
        switch (t) {
            case PP: return "PP";
            case HI: return "HI";
            case Cosmic: return "Cosmic";
            case Default:
            default: return "Default";
        }
    }

    const std::string RunTypeNames[] = {
            toString(Default), toString(PP), toString(HI), toString(Cosmic)};
}

#endif /* DQM_CONFIG_RUNTYPE_H_ */
