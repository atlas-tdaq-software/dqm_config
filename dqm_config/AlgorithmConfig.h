/* AlgorithmConfig.h
 Author Alina Corso-Radu 4/12/06
 */
#ifndef DQM_CONFIG_ALGORITHM_CONFIG_H
#define DQM_CONFIG_ALGORITHM_CONFIG_H

#include <string>
#include <vector>

#include <dqm_core/AlgorithmConfig.h>

#include <dqm_config/dal/DQParameter.h>
#include <dqm_config/is/DQParameter.h>
#include <dqm_config/AlgorithmConfigFactory.h>
#include <dqm_config/ConditionsManager.h>
#include <dqm_config/exceptions.h>

/*! \namespace dqm_config
 *  This is a wrapping namespace for all public classes of the DQM config .
 */
namespace dqm_config
{
    struct AlgorithmConfig: public virtual dqm_core::AlgorithmConfig
    {
        static const std::string ISServerName;

	AlgorithmConfig(const dal::DQParameter & c, AlgorithmConfigFactory & h);

	void updateFromDynamicSource(const is::DQParameter & config);

        const std::map<std::string, double> & getGreenThresholds() const override {
            return m_green[m_factory.getRunType()];
        }

        const std::map<std::string, double> & getRedThresholds() const override {
            return m_red[m_factory.getRunType()];
        }

    private:
        typedef std::vector<std::map<std::string, double>> Thresholds;

        void getThresholdsFromConfig(const std::string & parameter_name,
                const std::vector<std::string> & config, Thresholds & thrs);

        const AlgorithmConfigFactory & m_factory;
        std::string m_parameter_name;
        Thresholds m_red;
        Thresholds m_green;
    };

    template<typename... Policies>
    class AlgorithmConfigT: public AlgorithmConfig,
			    public Policies...
    {
    public:
	AlgorithmConfigT(const dal::DQParameter & c, AlgorithmConfigFactory & h)
	    : AlgorithmConfig(c, h),
	      Policies(c, h)...
	{ }
    };

    class ReferenceHandler: public virtual dqm_core::AlgorithmConfig
    {
    public:
	ReferenceHandler(const dal::DQParameter & config, AlgorithmConfigFactory & h);

	TObject * getReference() const override {
	    return m_refman.getReference(m_parameter_name);
	}

    private:
	std::string m_parameter_name;
	const ReferenceManager & m_refman;
    };

    class RunStateHandler: public virtual dqm_core::AlgorithmConfig
    {
    public:
	RunStateHandler(const dal::DQParameter & config, AlgorithmConfigFactory & h)
	    : m_run_state_watcher(h.getRunStateWatcher()),
	      m_active_in_physics(config.get_EnabledAt() == "Physics"),
	      m_disabled(config.get_EnabledAt() == "Never")
	{ }

	bool algorithmExecutionPermitted() const override {
	    return !m_disabled && m_active_in_physics == m_run_state_watcher.isPhysics();
	}

    private:
	const RunStateWatcher & m_run_state_watcher;
	const bool m_active_in_physics;
	const bool m_disabled;
    };

    class ConditionHandler: public virtual dqm_core::AlgorithmConfig
    {
    public:
	ConditionHandler(const dal::DQParameter & config, AlgorithmConfigFactory & h);

	const std::map<std::string, std::string> & getGenericParameters() const override;

	const std::map<std::string, double> & getParameters() const override;

    private:
	const ConditionsManager & m_conditions_manager;
	const std::vector<std::string> m_conditions_names;
    };
}

#endif // DQM_CONFIG_ALGORITHM_CONFIG_H
