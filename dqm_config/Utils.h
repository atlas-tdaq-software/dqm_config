/*
 * Utils.h
 *
 *  Created on: Oct 31, 2013
 *      Author: kolos
 */

#ifndef _DQM_CONFIG_UTILS_H_
#define _DQM_CONFIG_UTILS_H_

#include <string>
#include <vector>

namespace dqm_config {
namespace utils {
    typedef std::vector<std::pair<std::string, std::string>> Tokens;

    std::string replace(const std::string & in, const Tokens & tokens);

    std::vector<std::string> replace(const std::vector<std::string> & in, const Tokens & tokens);

    bool contains(const Tokens & set, const Tokens & subset);

    Tokens parse_tokens(const std::string & in);
}}


#endif
