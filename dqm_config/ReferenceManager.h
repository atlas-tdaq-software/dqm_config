/*
 * ReferenceManager.h
 *
 *  Created on: Nov 6, 2013
 *      Author: kolos
 */

#ifndef _DQM_CONFIG_REFERENCE_MANAGER_H_
#define _DQM_CONFIG_REFERENCE_MANAGER_H_

#include <string>
#include <unordered_map>

#include <oh/OHRootReceiver.h>
#include <oh/OHSubscriber.h>

#include <dqm_config/dal/DQParameter.h>
#include <dqm_config/RunType.h>

namespace dqm_config
{
    class ReferenceManager:
	protected OHRootReceiver,
	protected OHSubscriber
    {
	using OHRootReceiver::receive;

    public:
	enum Scope { StandBy, Physics, Always };

	ReferenceManager(const std::string & partition_name);

	void addReferences(const dal::DQParameter & config);

	TObject * getReference(const std::string & parameter_name) const;

	const std::string & getReferenceName(
	        const std::string & parameter_name, bool source = false) const;

	void publishReferences() const;

	void print(std::ostream & out) const;

        void setRunType(RunType t) {
            m_run_type = t;
        }

    protected:
	void receive(OHRootHistogram & h);
	void receive(OHRootGraph & h);
	void receive(OHRootGraph2D & h);

    private:
	void addReference(const dal::DQReference & ref,
	    const std::string & parameter_name,
	    const std::string & default_object_name,
	    std::vector<bool> & flags);

	struct Name { 
	    std::string m_physics;
	    std::string m_standby;
	    std::string m_physics_is;
	    std::string m_standby_is;
        };
	typedef std::unordered_map<std::string, std::shared_ptr<TObject>> Objects;
	typedef std::unordered_map<std::string, std::vector<Name>> Names;
	typedef std::unordered_map<std::string, std::vector<std::string>> Files;

    private:
	mutable std::mutex m_mutex;
	bool m_physics_mode;
	Objects m_objects;
	Names m_names;
	Files m_files;
        RunType m_run_type = RunType::Default;
    };
}

#endif /* _DQM_CONFIG_REFERENCE_MANAGER_H_ */
