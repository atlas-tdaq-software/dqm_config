/*
 * RunStateWatcher.h
 *
 *  Created on: Nov 14, 2013
 *      Author: kolos
 */

#ifndef _DQM_CONFIG_RUNSTATEWATCHER_H_
#define _DQM_CONFIG_RUNSTATEWATCHER_H_

#include <is/infowatcher.h>
#include <rc/Ready4PhysicsInfo.h>

namespace dqm_config
{
    class RunStateWatcher: public ISInfoWatcher<Ready4PhysicsInfo>
    {
    public:
	RunStateWatcher(const IPCPartition & p)
	  : ISInfoWatcher<Ready4PhysicsInfo>(p, "RunParams.Ready4Physics")
	{ ; }

	bool isPhysics() const{
	    return ready4physics;
	}
    };
}

#endif /* _DQM_CONFIG_RUNSTATEWATCHER_H_ */
