/* AlgorithmConfig.cpp
 Author Alina Corso-Radu 4/12/06
 Implements AlgorithmConfig abstract interface

 Reference histogram format:
 --------------------------

 1) if reference is read from root file there are two options: 

 a) path_to_root_file.root:Histogram_name

 Example: ${TDAQ_INST_PATH}/share/data/dqmf/references/ReferenceHistograms.root:CaloClusterVecMon/EtaPhi/EtaPhiEcut0

 b) path_to_root_file.root:

 In this case, the reference name and path  will be the same as the checked histogram path_name
 Example: ${TDAQ_INST_PATH}/share/data/dqmf/references/ReferenceHistograms.root:
 
 2) if reference is read from OH server:

 OH#ServerName.ProviderName.HistogramName

 Example: OH#Histogramming.LAr._EtaEcut0

 */

#include <boost/algorithm/string.hpp>

#include <ers/ers.h>
#include <dqm_config/AlgorithmConfig.h>
#include <dqm_config/dal/DQAlgorithm.h>
#include <dqm_config/dal/DQCondition.h>
#include <dqm_config/dal/DQParameter.h>
#include <dqm_config/dal/DQReference.h>

#include <is/infodictionary.h>
#include <oh/OHRootReceiver.h>

namespace
{
    void getParamsFromConfig(const std::string & parameter_name,
	const std::vector<std::string> & values,
	std::map<std::string, double> & params,
	std::map<std::string, std::string> * gen_params = 0)
    {
	for (size_t i = 0; i < values.size(); ++i)
	{
	    std::vector<std::string> key_val;
	    boost::split(key_val, values[i], boost::is_any_of("="));
	    if (key_val.size() != 2) {
                throw dqm_config::BadConfig(ERS_HERE, parameter_name,
                    "Bad value '" + values[i] + "' is set to the parameters");
	    }

            boost::trim(key_val[0]);
            boost::trim(key_val[1]);
            try {
                double d = boost::lexical_cast<double>(key_val[1]);
                params[key_val[0]] = d;
            }
            catch (boost::bad_lexical_cast & ex) {
                if (gen_params) {
                    (*gen_params)[key_val[0]] = key_val[1];
                }
                else {
                    throw dqm_config::BadConfig(ERS_HERE, parameter_name,
                        "Bad value is set to the '" + key_val[0] + "' parameter",
                        ex);
                }
            }
	}
    }
}

using namespace dqm_config;

const std::string AlgorithmConfig::ISServerName = "DQMConfig";

AlgorithmConfig::AlgorithmConfig(const dal::DQParameter & config,
        AlgorithmConfigFactory & h) :
        m_factory(h)
{
    try
    {
	m_parameter_name = config.UID();

        getThresholdsFromConfig(m_parameter_name, config.get_RedThresholds(),
            m_red);

        getThresholdsFromConfig(m_parameter_name, config.get_GreenThresholds(),
            m_green);

	getParamsFromConfig(m_parameter_name, config.get_Parameters(),
	    m_parameters,
	    &m_generic_parameters);
    }
    catch (daq::config::Generic & ex)
    {
	throw dqm_config::BadConfig(ERS_HERE, m_parameter_name, ex.what(), ex);
    }
}

void AlgorithmConfig::getThresholdsFromConfig(const std::string & parameter_name,
        const std::vector<std::string> & values, Thresholds & thresholds)
{
    thresholds.resize(std::size(RunTypeNames));
    for (size_t i = 0; i < values.size(); ++i)
    {
        std::vector<std::string> key_values;
        boost::split(key_values, values[i], boost::is_any_of("="));

        if (key_values.size() != 2) {
            throw dqm_config::BadConfig(ERS_HERE, parameter_name,
                "Bad value '" + values[i] + "' is set to the thresholds");
        }

        std::vector<std::string> values;
        boost::split(values, key_values[1], boost::is_any_of(";"));

        std::vector<bool> flags(std::size(RunTypeNames), false);
        for (size_t j = 0; j < values.size(); ++j) {
            std::vector<std::string> condition_value;
            boost::split(condition_value, values[j], boost::is_any_of(":"));
            if (condition_value.size() == 2) {
                for (size_t c = 0; c < std::size(RunTypeNames); ++c) {
                    if (boost::ifind_first(condition_value[0], RunTypeNames[c])) {
                        flags[c] = true;
                        thresholds[c][key_values[0]] = std::stof(condition_value[1]);
                        break;
                    }
                }
            } else {
                for (size_t c = 0; c < std::size(RunTypeNames); ++c) {
                    if (not flags[c]) {
                        flags[c] = true;
                        thresholds[c][key_values[0]] = std::stof(values[j]);
                    }
                }
            }
        }

        for (size_t c = 0; c < std::size(flags); ++c) {
            if (not flags[c]) {
                throw dqm_config::BadConfig(ERS_HERE, parameter_name,
                    "Threshold '" + key_values[0] +
                    "' doesn't contain a value for '" + RunTypeNames[c] + "' run type");
            }
        }
    }
}

void
AlgorithmConfig::updateFromDynamicSource(const is::DQParameter & config)
{
    std::unique_lock<AlgorithmConfig> lock(*this);

    try
    {
	getThresholdsFromConfig(m_parameter_name, config.RedThresholds,
	    m_red);

	getThresholdsFromConfig(m_parameter_name, config.GreenThresholds,
	    m_green);

	getParamsFromConfig(m_parameter_name, config.Parameters,
	    m_parameters,
	    &m_generic_parameters);
    }
    catch (ers::Issue & ex)
    {
	ers::error(ex);
    }
}

ReferenceHandler::ReferenceHandler(const dal::DQParameter & config, AlgorithmConfigFactory & h)
    : m_parameter_name(config.UID()),
      m_refman(h.getReferenceManager())
{
    try {
	h.getReferenceManager().addReferences(config);
    }
    catch (ers::Issue & ex) {
	ers::error(ex);
    }
}

ConditionHandler::ConditionHandler(const dal::DQParameter & config,
        AlgorithmConfigFactory & h)
  : m_conditions_manager(h.getConditionsManager())
{
    const std::vector<const dal::DQCondition*> & conditions =
            config.get_DQAlgorithm()->get_DQConditions();
    for (size_t i = 0; i < conditions.size(); ++i) {
	h.getConditionsManager().addCondition(
	        conditions[i]->UID(),
	        conditions[i]->get_Source(),
	        conditions[i]->get_DefaultValue());
    }
}

const std::map<std::string, std::string> &
ConditionHandler::getGenericParameters() const
{
    for (size_t i = 0; i < m_conditions_names.size(); ++i) {
	const std::string & n = m_conditions_names[i];
	m_conditions_manager.getConditionValue(n, m_generic_parameters[n]);
    }
    return m_generic_parameters;
}

const std::map<std::string, double> &
ConditionHandler::getParameters() const
{
    for (size_t i = 0; i < m_conditions_names.size(); ++i) {
	const std::string & n = m_conditions_names[i];
	m_conditions_manager.getConditionValue(n, m_parameters[n]);
    }
    return m_parameters;
}
