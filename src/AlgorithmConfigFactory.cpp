/*
 * AlgorithmConfigFactory.cpp
 *
 *  Created on: Nov 14, 2013
 *      Author: kolos
 */
#include <boost/algorithm/string.hpp>

#include <rc/RunParams.h>
#include <is/infodictionary.h>

#include <dqm_config/dal/DQAlgorithm.h>
#include <dqm_config/dal/DQParameter.h>
#include <dqm_config/dal/DQReference.h>

#include <dqm_config/AlgorithmConfig.h>
#include <dqm_config/AlgorithmConfigFactory.h>

dqm_config::AlgorithmConfigFactory::AlgorithmConfigFactory(const std::string & partition_name)
  : m_partition_name(partition_name),
    m_cman(partition_name),
    m_refman(partition_name),
    m_rs_watcher(partition_name),
    m_dynamic_data_source(partition_name, AlgorithmConfig::ISServerName, ".*",
	std::bind(&AlgorithmConfigFactory::valueReceived, this, std::placeholders::_1, std::placeholders::_2))
{
    updateRunParameters();
}

std::shared_ptr<dqm_config::AlgorithmConfig>
dqm_config::AlgorithmConfigFactory::createAlgorithmConfig(const dal::DQParameter & config)
{
    const std::string & name = config.UID();

    std::unique_lock < std::mutex > lock(m_mutex);
    Products::iterator it = m_products.find(name);
    if ( it != m_products.end()) {
	return it->second;
    }

    std::shared_ptr<AlgorithmConfig> ac(newAlgorithmConfig(config));
    m_products[name] = ac;

    DynamicData::iterator dit = m_dynamic_data.find(name);
    if ( dit != m_dynamic_data.end()) {
	ac->updateFromDynamicSource(dit->second);
	m_dynamic_data.erase(dit);
    }

    return ac;
}

dqm_config::AlgorithmConfig *
dqm_config::AlgorithmConfigFactory::newAlgorithmConfig(const dal::DQParameter & config)
{
    if (config.get_DQReferences().empty())
    {
	if (config.get_EnabledAt() == "Always")
	    if (config.get_DQAlgorithm()->get_DQConditions().empty())
		return new dqm_config::AlgorithmConfigT<>(config, *this);
	    else
		return new dqm_config::AlgorithmConfigT<ConditionHandler>(config, *this);
	else
	    if (config.get_DQAlgorithm()->get_DQConditions().empty())
		return new dqm_config::AlgorithmConfigT<RunStateHandler>(config, *this);
	    else
		return new dqm_config::AlgorithmConfigT<RunStateHandler, ConditionHandler>(config, *this);
    }
    else
    {
	if (config.get_EnabledAt() == "Always")
	    if (config.get_DQAlgorithm()->get_DQConditions().empty())
		return new dqm_config::AlgorithmConfigT<ReferenceHandler>(config, *this);
	    else
		return new dqm_config::AlgorithmConfigT<ReferenceHandler, ConditionHandler>(config, *this);
	else
	    if (config.get_DQAlgorithm()->get_DQConditions().empty())
		return new dqm_config::AlgorithmConfigT<ReferenceHandler, RunStateHandler>(config, *this);
	    else
		return new dqm_config::AlgorithmConfigT<ReferenceHandler, RunStateHandler, ConditionHandler>(config, *this);
    }
}

void
dqm_config::AlgorithmConfigFactory::valueReceived(const std::string & name, const dqm_config::is::DQParameter & config)
{
    ERS_DEBUG(1, "Dynamic configuration for the '" << name << "' parameter has been changed");
    std::unique_lock < std::mutex > lock(m_mutex);

    Products::iterator it = m_products.find(name);
    if ( it == m_products.end()) {
	m_dynamic_data[name] = config;
	return ;
    }

    it->second->updateFromDynamicSource(config);
    ERS_DEBUG(1, "Dynamic configuration for the '" << name << "' parameter has been updated");
}

void
dqm_config::AlgorithmConfigFactory::updateRunParameters() {
    // Get run parameters from IS
    try {
        RunParams rp;
        ISInfoDictionary d{IPCPartition(m_partition_name)};
        d.getValue("RunParams.RunParams", rp);
        dqm_config::RunType rtype = dqm_config::Default;
        if (boost::ifind_first(rp.run_type, "Physics")) {
            if (rp.beam_type >= 0 and rp.beam_type < 3) {
                rtype = static_cast<dqm_config::RunType>(rp.beam_type);
            }
        } else if (boost::ifind_first(rp.run_type, "Cosmic")) {
            rtype = dqm_config::Cosmic;
        }
        setRunType(rtype);
    }
    catch (ers::Issue& ex) {
       ers::error(ex);
    }
}

