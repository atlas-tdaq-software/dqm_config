/* DQParametersMethods.cpp
   Author Serguei Kolos 31/10/2013
   Implements methods of the DQM database class
*/

#include <dqm_config/dal/DQTemplateParameter.h>
#include <dqm_config/dal/DQTemplateReference.h>
#include <dqm_config/Utils.h>

const dqm_config::dal::DQParameter *
dqm_config::dal::DQTemplateParameter::newInstance(const std::string & string_tokens) const
{
    utils::Tokens tokens = utils::parse_tokens(string_tokens);

    DQParameter * parameter = new DQParameter(configuration(), config_object());
    parameter->init(false);
    parameter->p_UID = utils::replace(m_IDPattern, tokens);
    parameter->m_Description = utils::replace(m_Description, tokens);
    parameter->m_Troubleshooting = utils::replace(m_Troubleshooting, tokens);
    parameter->m_DrawOption = utils::replace(m_DrawOption, tokens);
    parameter->m_CanvasOption = utils::replace(m_CanvasOption, tokens);
    parameter->m_Label = utils::replace(m_Label, tokens);
    parameter->m_InputDataSource = utils::replace(m_InputDataSource, tokens);
    parameter->m_Parameters = utils::replace(m_Parameters, tokens);
    parameter->m_RedThresholds = utils::replace(m_RedThresholds, tokens);
    parameter->m_GreenThresholds = utils::replace(m_GreenThresholds, tokens);

    const std::vector<const DQReference *> & rr = m_DQReferences;
    for (size_t i = 0; i < rr.size(); i++)
    {
	const DQTemplateReference * tmpl = rr[i]->cast<DQTemplateReference>();
	if (tmpl)
	    parameter->m_DQReferences[i] = tmpl->get_instance(string_tokens);
	else
	    parameter->m_DQReferences[i] = rr[i];
    }

    if (!m_DisabledInstances.empty()) {
	for (size_t i = 0; i < m_DisabledInstances.size(); ++i)
	{
	    utils::Tokens disabled = utils::parse_tokens(m_DisabledInstances[i]);
	    if (utils::contains(tokens, disabled))
	    {
		parameter->m_EnabledAt = "Never";
		break;
	    }
	}
    }

    ERS_DEBUG(3, "The '" << parameter->p_UID << "' DQParameter was created with the '"
	<< string_tokens << "' tokens");

    return parameter;
}

const std::vector<const dqm_config::dal::DQParameter *> &
dqm_config::dal::DQTemplateParameter::get_instances(const std::string & tokens) const
{
    check_init();

    std::vector<const DQParameter *> * result;
    const std::vector<std::string> & instances = get_Instances();
    {
	std::lock_guard<std::mutex> guard(m_mutex);

	std::map<std::string, std::vector<const DQParameter *>>::iterator it = m_instances.find(tokens);
	if (it != m_instances.end())
	{
	    result = &it->second;
	}
	else
	{
	    result =
		&(m_instances.insert(std::make_pair(tokens, std::vector<const DQParameter *>())).first->second);

	    for ( size_t i = 0; i < instances.size(); ++i )
		result->push_back(newInstance(tokens + "," + instances[i]));
	}
    }

    return *result;
}
