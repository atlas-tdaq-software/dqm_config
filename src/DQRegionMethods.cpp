/* DQRegionMethods.cpp
 Author Serguei Kolos 31/01/2007
 Implements methods of the DQRegionMethods database class
 */
#include <functional>

#include <dal/app-config.h>
#include <dal/Partition.h>

#include <dqm_config/dal/DQAgent.h>
#include <dqm_config/dal/DQAlgorithm.h>
#include <dqm_config/dal/DQRegion.h>
#include <dqm_config/dal/DQParameter.h>
#include <dqm_config/dal/DQTemplateParameter.h>
#include <dqm_config/dal/DQTemplateRegion.h>

namespace
{
    bool
    enumerate_libraries(const dqm_config::dal::DQRegion * region,
	std::vector<std::string> & libs)
    {
	const std::vector<const dqm_config::dal::DQParameter *> & parameters =
	    region->get_DQParameters();
	for (size_t i = 0; i < parameters.size(); i++)
	{
	    std::string libname = parameters[i]->get_DQAlgorithm()->get_LibraryName();
	    std::vector<std::string>::iterator found = std::find(libs.begin(), libs.end(),
		libname);
	    if (found != libs.end())
	    {
		libs.push_back(libname);
	    }
	}
	return true;
    }

    void get_top_parents(Configuration & config,
	const dqm_config::dal::DQRegion * region,
	std::vector<const dqm_config::dal::DQRegion*> & top_parents)
    {
	std::vector<const dqm_config::dal::DQRegion*> other_regions;
	config.referenced_by(*region, other_regions, "DQRegions");

	if (other_regions.empty())
	{
	    top_parents.push_back(region);
	}
	else
	{
	    for (size_t i = 0; i < other_regions.size(); ++i)
	    {
		get_top_parents(config, other_regions[i], top_parents);
	    }
	}
    }
}

void
dqm_config::dal::DQRegion::visit_all_regions(
    std::function<bool(const dqm_config::dal::DQRegion *)> visitor) const
    {
    if (!visitor(this))
	return;

    std::vector<const dqm_config::dal::DQRegion*> regions;
    get_all_regions(regions);
    for (size_t i = 0; i < regions.size(); i++)
	regions[i]->visit_all_regions(visitor);
}

void
dqm_config::dal::DQRegion::visit_regions(
    std::function<bool(const dqm_config::dal::DQRegion *)> visitor) const
    {
    if (!visitor(this))
	return;

    const std::vector<const dqm_config::dal::DQRegion*> & regions = get_DQRegions();
    for (size_t i = 0; i < regions.size(); i++)
	regions[i]->visit_regions(visitor);
}

void
dqm_config::dal::DQRegion::get_algorithms_libraries(std::vector<std::string> & libs) const
{
    visit_all_regions(
	std::bind(&::enumerate_libraries, std::placeholders::_1, std::ref(libs)));
}

void
dqm_config::dal::DQRegion::get_root_regions(
    Configuration & config,
    const daq::core::Partition & partition,
    std::vector<const dqm_config::dal::DQRegion *> & result)
{
    std::vector<const dqm_config::dal::DQRegion *> root_regions;
    std::set<std::string> type = {"DQAgent"};
    std::vector<const daq::core::BaseApplication*> agents = partition.get_all_applications(&type);

    std::vector<const DQRegion*> non_top_regions;
    for (std::vector<const daq::core::BaseApplication*>::const_iterator it = agents.begin();
	it != agents.end(); ++it)
    {
	const DQAgent * agent = config.cast<DQAgent>(*it);
	if (!agent) {
	    continue;
	}
	const std::vector<const DQRegion*> & regions = agent->get_DQRegions();
	for (size_t i = 0; i < regions.size(); ++i)
	{
	    std::vector<const DQRegion*> other_regions;
	    config.referenced_by(*regions[i], other_regions, "DQRegions");
	    if (other_regions.empty()) {
		root_regions.push_back(regions[i]);
	    }
	    else {
                non_top_regions.push_back(regions[i]);
	    }
	}
    }

    for (size_t i = 0; i < non_top_regions.size(); ++i)
    {
	std::vector<const DQRegion*> top_parents;
	get_top_parents(config, non_top_regions[i], top_parents);

	for (size_t j = 0; j < top_parents.size(); ++j)
	{
	    if (std::find(root_regions.begin(), root_regions.end(), top_parents[j])
		== root_regions.end())
	    {
		root_regions.push_back(non_top_regions[i]);
	    }
	}
    }

    for ( size_t i = 0; i < root_regions.size(); i++ )
    {
        const DQTemplateRegion * tmpl = root_regions[i]->cast<DQTemplateRegion>();
        if (!tmpl)
        {
            result.push_back(root_regions[i]);
        }
        else
        {
            const std::vector<const dqm_config::dal::DQRegion *> & g = tmpl->get_instances("");
            result.insert(result.end(), g.begin(), g.end());
        }
    }
}

bool
dqm_config::dal::DQRegion::is_referenced_by_agent() const
{
    std::ostringstream out;
    out << "(this (\"DQRegions\" some ( object-id \"" << config_object().UID() << "\" =)))";
    std::vector<const dqm_config::dal::DQAgent *> agents;
    configuration().get(agents, false, false, out.str());
    return (agents.size() != 0);
}

void
dqm_config::dal::DQRegion::get_referencing_agents(
    std::vector<const dqm_config::dal::DQAgent*> & agents) const
    {
    std::ostringstream out;
    out << "(this (\"DQRegions\" some ( object-id \"" << config_object().UID() << "\" =)))";
    configuration().get(agents, false, false, out.str());
}

void
dqm_config::dal::DQRegion::get_all_parameters(
    std::vector<const dqm_config::dal::DQParameter *> & parameters) const
    {
    const std::vector<const dqm_config::dal::DQParameter *> & pp = get_DQParameters();

    for (size_t i = 0; i < pp.size(); i++)
    {
	const DQTemplateParameter * tmpl = pp[i]->cast<DQTemplateParameter>();
	if (!tmpl)
	{
	    ERS_DEBUG(3, "Processing '" << pp[i]->UID() << "' DQ Parameter");
	    parameters.push_back(pp[i]);
	}
	else
	{
	    ERS_DEBUG(3, "Processing '" << pp[i]->UID() << "' DQ Template Parameter");
	    const std::vector<const dqm_config::dal::DQParameter *> & g =
		tmpl->get_instances(m_tokens);
	    ERS_DEBUG(3,
		g.size() << " instances have been generated for the '" << pp[i]->UID()
		    << "' DQ Template Parameter");
	    parameters.insert(parameters.end(), g.begin(), g.end());
	}
    }
}

void
dqm_config::dal::DQRegion::get_all_regions(
    std::vector<const dqm_config::dal::DQRegion *> & regions) const
    {
    ERS_DEBUG(3, "Processing '" << UID() << "' DQ Region");

    const std::vector<const dqm_config::dal::DQRegion *> & rr = get_DQRegions();

    for (size_t i = 0; i < rr.size(); i++)
    {
	const DQTemplateRegion * tmpl = rr[i]->cast<DQTemplateRegion>();
	if (!tmpl)
	{
	    ERS_DEBUG(3, "Processing '" << rr[i]->UID() << "' child DQ Region");
	    regions.push_back(rr[i]);
	}
	else
	{
	    ERS_DEBUG(3, "Processing '" << rr[i]->UID() << "' child DQ Template Region");
	    const std::vector<const dqm_config::dal::DQRegion *> & g =
		tmpl->get_instances(m_tokens);
	    ERS_DEBUG(3,
		g.size() << " instances have been generated for the '" << rr[i]->UID()
		    << "' DQ Template Region");
	    regions.insert(regions.end(), g.begin(), g.end());
	}
    }
}
