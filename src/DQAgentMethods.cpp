/* DQAgentMethods.cpp
   Author Serguei Kolos 31/01/2007
   Implements methods of the DQAgent database class
*/

#include <dqm_config/dal/DQAgent.h>
#include <dqm_config/dal/DQAlgorithm.h>
#include <dqm_config/dal/DQRegion.h>
#include <dqm_config/dal/DQParameter.h>
#include <dqm_config/dal/DQTemplateRegion.h>

void
dqm_config::dal::DQAgent::get_algorithms_libraries( std::vector<std::string> & libs ) const
{
    const std::vector<const dqm_config::dal::DQRegion *> & regions = get_DQRegions();
    
    for ( size_t i = 1; i < regions.size(); i++ )
    {
	regions[i]->get_algorithms_libraries( libs );
    }
}

void
dqm_config::dal::DQAgent::get_all_regions(
    std::vector<const dqm_config::dal::DQRegion *> & regions) const
{
    ERS_DEBUG(2, "Processing '" << UID() << "' DQ Agent");

    const std::vector<const dqm_config::dal::DQRegion *> & rr = get_DQRegions();

    for ( size_t i = 0; i < rr.size(); i++ )
    {
	const DQTemplateRegion * tmpl = rr[i]->cast<DQTemplateRegion>();
	if (!tmpl)
	{
	    ERS_DEBUG(2, "Processing '" << rr[i]->UID() << "' child DQ Region");
	    regions.push_back(rr[i]);
	}
	else
	{
	    ERS_DEBUG(2, "Processing '" << rr[i]->UID() << "' child DQ Template Region");
	    const std::vector<const dqm_config::dal::DQRegion *> & g = tmpl->get_instances("");
	    ERS_DEBUG(2, g.size() << " instances have been generated for the '" << rr[i]->UID() << "' DQ Template Region");
	    regions.insert(regions.end(), g.begin(), g.end());
	}
    }
}

void
dqm_config::dal::DQAgent::visit_all_regions(std::function<bool (const dqm_config::dal::DQRegion *)> visitor) const
{
    std::vector<const dqm_config::dal::DQRegion*> regions;
    get_all_regions(regions);
    for ( size_t i = 0; i < regions.size(); i++ )
	regions[i]->visit_all_regions(visitor);
}

void
dqm_config::dal::DQAgent::visit_regions(std::function<bool (const dqm_config::dal::DQRegion *)> visitor) const
{
    const std::vector<const dqm_config::dal::DQRegion*> & regions = get_DQRegions();
    for ( size_t i = 0; i < regions.size(); i++ )
	regions[i]->visit_regions(visitor);
}

