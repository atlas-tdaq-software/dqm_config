/* DQParametersMethods.cpp
   Author Serguei Kolos 31/10/2013
   Implements methods of the DQM database class
*/

#include <dqm_config/dal/DQTemplateReference.h>
#include <dqm_config/Utils.h>

const dqm_config::dal::DQReference *
dqm_config::dal::DQTemplateReference::newInstance(const std::string & string_tokens) const
{
    utils::Tokens tokens = utils::parse_tokens(string_tokens);

    DQReference * reference = new DQReference(configuration(), config_object());
    reference->init(false);
    reference->p_UID = p_UID + "<" + string_tokens + ">";
    reference->m_Source = utils::replace(m_Source, tokens);

    ERS_DEBUG(3, "The '" << reference->p_UID << "' DQReference is created with the '"
	<< string_tokens << "' tokens");

    return reference;
}

const dqm_config::dal::DQReference *
dqm_config::dal::DQTemplateReference::get_instance(const std::string & tokens) const
{
    check_init();

    std::lock_guard<std::mutex> guard(m_mutex);

    std::map<std::string, const DQReference *>::iterator it = m_instances.find(tokens);
    if (it != m_instances.end())
	return it->second;
    else
	return m_instances.insert(std::make_pair(tokens, newInstance(tokens))).first->second;
}
