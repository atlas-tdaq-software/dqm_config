/* Utils.cpp
   Author Serguei Kolos 31/10/2013
   Implements methods of the DQM database class
*/
#include <algorithm>
#include <functional>
#include <vector>

#include <boost/algorithm/string.hpp>

#include <ers/ers.h>

#include <dqm_config/Utils.h>

namespace {
    void
    replaceAll(std::string& str, const std::string& from, const std::string& to)
    {
        size_t start_pos = 0;
        while((start_pos = str.find(from, start_pos)) != std::string::npos) {
            str.replace(start_pos, from.length(), to);
            start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
        }
    }
}

namespace dqm_config {
namespace utils {

    bool contains(const Tokens & set, const Tokens & subset)
    {
	size_t matches = 0;
	for ( size_t i = 0; i < subset.size(); ++i )
	{
	    for ( size_t j = 0; j < set.size(); ++j )
	    {
		if (subset[i] == set[j]) {
		    ++matches;
		    break;
		}
	    }

	    if (matches == i)
		return false;
	}

	return true;
    }

    Tokens parse_tokens(const std::string & in)
    {
	Tokens tokens;
	std::vector<std::string> pairs;
	boost::split(pairs, in, boost::is_any_of(","));
	for ( size_t i = 0; i < pairs.size(); ++i )
	{
	    std::vector<std::string> key_val;
	    boost::split(key_val, pairs[i], boost::is_any_of("="));
	    if (key_val.size() == 2) {
		boost::trim(key_val[0]);
		boost::trim(key_val[1]);
		tokens.push_back(std::make_pair(key_val[0], key_val[1]));
	    }
	}
	return tokens;
    }

    std::string
    replace(const std::string & in, const Tokens & tokens)
    {
	std::string s(in);

	for ( size_t i = 0; i < tokens.size(); ++i )
	{
	    replaceAll(s, "{" + tokens[i].first + "}", tokens[i].second);
	}
	return s;
    }

    std::vector<std::string>
    replace(const std::vector<std::string> & in, const Tokens & tokens)
    {
	std::vector<std::string> v;
	std::vector<std::string>::const_iterator it = in.begin();
	for ( ; it != in.end(); ++it )
	{
	    v.push_back(replace(*it, tokens));
	}
	return v;
    }
}}
