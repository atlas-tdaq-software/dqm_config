/*
 * ConditionSource.cpp
 *
 *  Created on: Nov 12, 2013
 *      Author: kolos
 */

#include <boost/lexical_cast.hpp>

#include <dqm_config/ConditionSource.h>

dqm_config::Condition::Condition(const std::string & n,
    const std::string & a, const std::string & d)
    : m_name(n),
      m_attribute_name(a)
{
    setValue(d);
}

void
dqm_config::Condition::setValue(const std::string & v) const
{
    m_string_value = v;
    try
    {
	m_double_value = boost::lexical_cast<double>(m_string_value);
	m_is_numeric = true;
    }
    catch (boost::bad_lexical_cast &)
    {
	m_is_numeric = false;
    }
}

void
dqm_config::Condition::setValue(double v) const
{
    m_double_value = v;
    m_is_numeric = true;
    m_string_value = boost::lexical_cast < std::string > (m_double_value);
}

dqm_config::ConditionSource::ConditionSource(
    const IPCPartition & p, const std::string & object_name)
    : m_info_watcher(p, object_name,
	std::bind(&dqm_config::ConditionSource::valueReceived, this, std::placeholders::_1, std::placeholders::_2))
{
    ;
}

void
dqm_config::ConditionSource::addCondition(const std::string & condition_name,
    const std::string & attribute_name, const std::string & default_value)
{
    std::unique_lock < std::mutex > lock(m_mutex);
    Conditions::iterator it = m_conditions.find(condition_name);
    if (it == m_conditions.end())
	m_conditions.insert(Condition(condition_name, attribute_name, default_value));
}

bool
dqm_config::ConditionSource::getConditionValue(const std::string & condition_name,
    double & value) const
{
    std::unique_lock < std::mutex > lock(m_mutex);
    Conditions::const_iterator it = m_conditions.find(condition_name);
    if (it != m_conditions.end() && it->m_is_numeric)
    {
	value = it->m_double_value;
	return true;
    }

    return false;
}

bool
dqm_config::ConditionSource::getConditionValue(const std::string & condition_name,
    std::string & value) const
{
    std::unique_lock < std::mutex > lock(m_mutex);
    Conditions::const_iterator it = m_conditions.find(condition_name);
    if (it != m_conditions.end())
    {
	value = it->m_string_value;
	return true;
    }

    return false;
}

void
dqm_config::ConditionSource::valueReceived(const std::string & name, const ISInfoDynAny & info)
{
    std::unique_lock < std::mutex > lock(m_mutex);
    Conditions::iterator it = m_conditions.begin();
    for (; it != m_conditions.end(); ++it)
    {
	try
	{
	    ISType::Basic t = info.getAttributeType(it->m_attribute_name);
	    if (t == ISType::String)
	    {
		it->setValue(info.getAttributeValue<std::string>(it->m_attribute_name));
	    }
	    else
	    {
		double v;
		switch (t)
		{
		    case ISType::S8:
			v = info.getAttributeValue<char>(it->m_attribute_name);
			break;
		    case ISType::U8:
			v = info.getAttributeValue<unsigned char>(it->m_attribute_name);
			break;
		    case ISType::S16:
			v = info.getAttributeValue<short>(it->m_attribute_name);
			break;
		    case ISType::U16:
			v = info.getAttributeValue<unsigned short>(it->m_attribute_name);
			break;
		    case ISType::S32:
			v = info.getAttributeValue<int>(it->m_attribute_name);
			break;
		    case ISType::U32:
			v = info.getAttributeValue<unsigned int>(it->m_attribute_name);
			break;
		    case ISType::S64:
			v = info.getAttributeValue < int64_t > (it->m_attribute_name);
			break;
		    case ISType::U64:
			v = info.getAttributeValue < uint64_t > (it->m_attribute_name);
			break;
		    default:
			return;
		}
		it->setValue(v);
	    }
	}
	catch (daq::is::Exception & ex)
	{
	    ERS_DEBUG(1,
		"IS object '" << name << "' does not have the '" << it->m_attribute_name
		    << "' attribute: " << ex);
	}
    }
}

