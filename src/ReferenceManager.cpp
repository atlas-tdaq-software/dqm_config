/* ReferenceManager.cpp

 Author Serguei Kolos 4/12/13

 */

#include <TFile.h>
#include <TH1.h>
#include <TGraph.h>

#include <ers/ers.h>

#include <is/infodictionary.h>
#include <oh/OHRootProvider.h>
#include <oh/OHUtil.h>

#include <dqm_core/exceptions.h>

#include <dqm_config/exceptions.h>
#include <dqm_config/AlgorithmConfig.h>
#include <dqm_config/ReferenceManager.h>
#include <dqm_config/dal/DQReference.h>

namespace {
    dqm_config::ReferenceManager::Scope toScope(const std::string & scope)
    {
        if (scope == "StandBy")
            return dqm_config::ReferenceManager::StandBy;
        else if (scope == "Physics")
            return dqm_config::ReferenceManager::Physics;
        else
            return dqm_config::ReferenceManager::Always;
    }

    dqm_config::RunType toRunType(const std::string & rt)
    {
        if (rt == "Cosmic")
            return dqm_config::RunType::Cosmic;
        else if (rt == "PP")
            return dqm_config::RunType::PP;
        else if (rt == "HI")
            return dqm_config::RunType::HI;
        else
            return dqm_config::RunType::Default;
    }
}

dqm_config::ReferenceManager::ReferenceManager(const std::string & partition_name)
    : OHSubscriber(partition_name, *this, false),
      m_physics_mode(false)
{ }

void
dqm_config::ReferenceManager::publishReferences() const
{
    Files::const_iterator it = m_files.begin();
    for ( ; it != m_files.end(); ++it ) {
	std::shared_ptr<TFile> file(TFile::Open(it->first.c_str()));

	if (!file) {
	    ers::error(dqm_config::CantOpenRootFile(ERS_HERE, it->first));
	    continue;
	}

	std::string provider_name(it->first);
	std::replace(provider_name.begin(), provider_name.end(), '.', '_');

	try {
	    OHRootProvider provider(partition(), AlgorithmConfig::ISServerName, provider_name);

	    for (size_t i = 0; i < it->second.size(); ++i) {
		TObject * object = file->Get(it->second[i].c_str());

		if( !object || object->InheritsFrom(TDirectoryFile::Class()))
		{
		    ers::error(dqm_config::CantReadReference(ERS_HERE, it->second[i], it->first));
		    continue;
		}

		// publish histograms to IS
		if ( object->InheritsFrom(TH1::Class()) ) {
		    provider.publish(*((TH1*)object), it->second[i]);
		}
		else if ( object->InheritsFrom(TGraph::Class()) ) {
		    provider.publish(*((TGraph*)object), it->second[i]);
		}
		else if ( object->InheritsFrom(TGraph2D::Class()) ) {
		    provider.publish(*((TGraph2D*)object), it->second[i]);
		}
	    }
	}
	catch (ers::Issue & ex) {
	    ers::error(ex);
	}
    }
}

void
dqm_config::ReferenceManager::addReferences(const dal::DQParameter & config)
{
    try {
	const std::vector<const dal::DQReference*> & references = config.get_DQReferences();

	if (references.empty()) {
	    return;
	}

        std::vector<Name> & names = m_names[config.UID()];
        names.resize(std::size(RunTypeNames));

        std::vector<bool> flags(std::size(RunTypeNames), false);
        std::string default_object_name =
                oh::util::get_object_name(config.get_InputDataSource()[0]);

	for (auto it = references.begin(); it != references.end(); it++) {
	    addReference(*(*it), config.UID(), default_object_name, flags);
	}

        if (not flags[RunType::Default]) {
            throw dqm_config::BadConfig(ERS_HERE, config.UID(),
                "No default reference is defined");
        }

        for (size_t i = 1; i < names.size(); ++i) {
            if (not flags[i]) {
                names[i] = names[RunType::Default];
            }
        }
    }
    catch (ers::Issue & ex) {
	ers::error(dqm_config::BadConfig(ERS_HERE, config.UID(), ex.what(), ex));
    }
}

void
dqm_config::ReferenceManager::addReference(const dal::DQReference & ref,
        const std::string & parameter_name, const std::string & default_object_name,
        std::vector<bool> & flags)
{
    std::unique_lock<std::mutex> lock(m_mutex);

    std::string is_object_name = ref.get_is_name(default_object_name);
    Objects::const_iterator it = m_objects.find(is_object_name);
    if (it == m_objects.end())
    {
        TObject * tobj = 0;
	try {
            OHRootObject ro = OHRootReceiver::getRootObject(partition(), is_object_name);
            tobj = ro.object.release();
        }
        catch (ers::Issue & ex) {
            ;
        }

        m_objects.insert(std::make_pair(is_object_name, std::shared_ptr<TObject>(tobj)));
	try {
	    subscribe(is_object_name);
	}
	catch (ers::Issue & ex) {
	    ers::error(dqm_config::BadConfig(ERS_HERE, parameter_name, ex.what(), ex));
	}
    }

    Scope scope = toScope(ref.get_ValidAt());
    RunType rtype = toRunType(ref.get_ValidFor());
    std::string source = ref.get_source(default_object_name);

    std::vector<Name> & names = m_names[parameter_name];
    flags[rtype] = true;

    Name & n = names[rtype];
    if (scope == Always)
    {
	if (n.m_physics.empty()) {
	    n.m_physics = source;
	    n.m_physics_is = is_object_name;
	}
	if (n.m_standby.empty()) {
	    n.m_standby = source;
	    n.m_standby_is = is_object_name;
	}
    }
    else if (scope == Physics)
    {
	n.m_physics = source;
        n.m_physics_is = is_object_name;
    }
    else
    {
	n.m_standby = source;
        n.m_standby_is = is_object_name;
    }

    std::string file_name;
    std::string object_name = default_object_name;
    ref.get_file_and_name(file_name, object_name);
    
    if (ref.requires_publication()) {
	Files::iterator it = m_files.find(file_name);
	if (it != m_files.end()) {
	    it->second.push_back(object_name);
	}
	else {
	    m_files.insert(std::make_pair(file_name, std::vector<std::string>(1, object_name)));
	}
    }
}

TObject *
dqm_config::ReferenceManager::getReference(const std::string & parameter_name) const
{
    std::unique_lock<std::mutex> lock(m_mutex);
    
    Names::const_iterator it = m_names.find(parameter_name);
    
    if (it != m_names.end())
    {
	const std::vector<Name> & names = it->second;

	RunType index = m_run_type;
	if (index >= names.size()) {
	    index = RunType::Default;
	}

	std::string name = m_physics_mode ? names[index].m_physics_is
	        : names[index].m_standby_is;

        if (name.empty()) {
            throw dqm_core::BadConfig(ERS_HERE, parameter_name,
                    "No reference defined for " + toString(index) + " run type");
        }

	Objects::const_iterator it = m_objects.find(name);

	if (it != m_objects.end() && it->second) {
	    return it->second.get();
	}
    }
    
    throw dqm_core::BadConfig(ERS_HERE, parameter_name,
            "No references defined for this parameter");
}

const std::string &
dqm_config::ReferenceManager::getReferenceName(
        const std::string & parameter_name, bool source) const
{
    std::unique_lock<std::mutex> lock(m_mutex);

    Names::const_iterator it = m_names.find(parameter_name);
    if (it != m_names.end())
    {
        const std::vector<Name> & names = it->second;

        if (m_run_type < names.size()) {
            if (source) {
                return m_physics_mode ? names[m_run_type].m_physics
                        : names[m_run_type].m_standby;
            } else {
                return m_physics_mode ? names[m_run_type].m_physics_is
                        : names[m_run_type].m_standby_is;
            }
       }
    }

    static std::string empty;
    return empty;
}

void
dqm_config::ReferenceManager::print(std::ostream & out) const
{
    Names::const_iterator it = m_names.begin();

    for ( ; it != m_names.end(); ++it)
    {
	out << "Parameter '" << it->first << "' has references {" << std::endl;
	for (size_t i = 0; i < it->second.size(); ++i) {
	    out << "\t " << toString(static_cast<RunType>(i)) << " {" << std::endl;
	    out << "\t   in physics mode '" << it->second[i].m_physics << "'" << std::endl;
	    out << "\t   in standby mode '" << it->second[i].m_standby << "'" << std::endl;
	    out << "\t}" << std::endl;
	}
	out << "}" << std::endl;
    }
}

void dqm_config::ReferenceManager::receive(OHRootHistogram& h)
{
    std::string name = h.histogram->GetName();

    std::unique_lock<std::mutex> lock(m_mutex);
    m_objects[name] = std::shared_ptr<TObject>(h.histogram.release());
}

void dqm_config::ReferenceManager::receive(OHRootGraph& h)
{
    std::string name = h.graph->GetName();

    std::unique_lock<std::mutex> lock(m_mutex);
    m_objects[name] = std::shared_ptr<TObject>(h.graph.release());
}

void dqm_config::ReferenceManager::receive(OHRootGraph2D& h)
{
    std::string name = h.graph->GetName();

    std::unique_lock<std::mutex> lock(m_mutex);
    m_objects[name] = std::shared_ptr<TObject>(h.graph.release());
}
