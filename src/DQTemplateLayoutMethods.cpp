/* DQTemplateLayout.cpp
   Author Serguei Kolos 31/10/2013
   Implements methods of the DQM database class
*/

#include <dqm_config/dal/DQTemplateLayout.h>
#include <dqm_config/Utils.h>

const dqm_config::dal::DQLayout *
dqm_config::dal::DQTemplateLayout::newInstance(const std::string & string_tokens) const
{
    utils::Tokens tokens = utils::parse_tokens(string_tokens);

    DQLayout * layout = new DQLayout(configuration(), config_object());
    layout->init(false);
    layout->p_UID = p_UID + "<" + string_tokens + ">";
    layout->m_RowHeaders = utils::replace(m_RowHeaders, tokens);
    layout->m_ColumnHeaders = utils::replace(m_ColumnHeaders, tokens);
    layout->m_Labels = utils::replace(m_Labels, tokens);

    ERS_DEBUG(3, "The '" << layout->p_UID << "' DQLayout is created with the '"
	<< string_tokens << "' tokens");

    return layout;
}

const dqm_config::dal::DQLayout *
dqm_config::dal::DQTemplateLayout::get_instance(const std::string & tokens) const
{
    check_init();

    std::lock_guard<std::mutex> guard(m_mutex);

    std::map<std::string, const DQLayout *>::iterator it = m_instances.find(tokens);
    if (it != m_instances.end())
	return it->second;
    else
	return m_instances.insert(std::make_pair(tokens, newInstance(tokens))).first->second;
}
