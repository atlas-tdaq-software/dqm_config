/* DQTemplateRegionMethods.cpp
   Author Serguei Kolos 31/10/2013
   Implements methods of the DQM database class
*/

#include <dqm_config/dal/DQTemplateLayout.h>
#include <dqm_config/dal/DQTemplateRegion.h>
#include <dqm_config/Utils.h>

const dqm_config::dal::DQRegion *
dqm_config::dal::DQTemplateRegion::newInstance(const std::string & string_tokens) const
{
    utils::Tokens tokens = utils::parse_tokens(string_tokens);

    DQRegion * region = new DQRegion(configuration(), config_object());
    region->init(false);
    region->p_UID = utils::replace(m_IDPattern, tokens);
    region->m_Label = utils::replace(m_Label, tokens);
    region->m_Description = utils::replace(m_Description, tokens);
    region->m_Troubleshooting = utils::replace(m_Troubleshooting, tokens);
    region->m_tokens = string_tokens;

    const DQTemplateLayout * tmpl = m_DQLayout ? m_DQLayout->cast<DQTemplateLayout>() : 0;
    if (tmpl)
	region->m_DQLayout = tmpl->get_instance(string_tokens);

    if (!m_DisabledInstances.empty()) {
	for (size_t i = 0; i < m_DisabledInstances.size(); ++i)
	{
	    utils::Tokens disabled = utils::parse_tokens(m_DisabledInstances[i]);
	    if (utils::contains(tokens, disabled))
	    {
		region->m_EnabledAt = "Never";
		break;
	    }
	}
    }

    ERS_DEBUG(3, "The '" << region->p_UID << "' DQ Region was created with the '"
	<< string_tokens << "' parameter");
    return region;
}

const std::vector<const dqm_config::dal::DQRegion *> &
dqm_config::dal::DQTemplateRegion::get_instances(const std::string & string_tokens) const
{
    check_init();

    std::vector<const DQRegion *> * result;
    const std::vector<std::string> & instances = get_Instances();
    {
	std::lock_guard<std::mutex> guard(m_mutex);

	std::map<std::string, std::vector<const DQRegion *>>::iterator it = m_instances.find(string_tokens);
	if (it != m_instances.end())
	{
	    result = &it->second;
	}
	else
	{
	    result =
		&(m_instances.insert(std::make_pair(string_tokens, std::vector<const DQRegion *>())).first->second);

	    for ( size_t i = 0; i < instances.size(); ++i )
		result->push_back(newInstance(string_tokens + "," + instances[i]));
	}
    }
    return *result;
}
