/* DQParameterMethods.cpp
   Author Serguei Kolos 4/12/06
*/

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#include <dqm_config/dal/DQParameter.h>

#include <dqm_config/exceptions.h>

namespace
{
    void
    fromConfig(	const std::string & parameter_name,
		const std::vector<std::string> & values,
		std::map<std::string, double> & params)
    {
	for (size_t i = 0; i < values.size(); ++i)
	{
	    std::vector<std::string> key_val;
	    boost::split(key_val, values[i], boost::is_any_of("="));
	    if (key_val.size() == 2) {
		boost::trim(key_val[0]);
		boost::trim(key_val[1]);
		try {
		    params.insert(std::make_pair(key_val[0], boost::lexical_cast<double>(key_val[1])));
		}
		catch(boost::bad_lexical_cast & ex) {
		    ers::debug( dqm_config::BadConfig( ERS_HERE, parameter_name,
			    "Bad value is given to the '" + key_val[0] + "' attribute", ex ), 1);
		}
	    }
	}
    }
}

void
dqm_config::dal::DQParameter::get_parameters(std::map<std::string, double> & params) const
{
    fromConfig(UID(), get_Parameters(), params);
}

void
dqm_config::dal::DQParameter::get_red_thresholds(std::map<std::string, double> & thr) const
{
    fromConfig(UID(), get_RedThresholds(), thr);
}

void
dqm_config::dal::DQParameter::get_green_thresholds(std::map<std::string, double> & thr) const
{
    fromConfig(UID(), get_GreenThresholds(), thr);
}
