/*
 * ConditionsManager.cpp
 *
 *  Created on: Nov 12, 2013
 *      Author: kolos
 */

#include <dqm_config/ConditionsManager.h>

namespace
{
    std::string get_attribute_name(const std::string & name)
    {
	std::string::size_type pos = name.rfind('.');
	if (pos != std::string::npos)
	    return name.substr(pos+1);
	return std::string();
    }

    std::string get_info_name(const std::string & name)
    {
	std::string::size_type pos = name.rfind('.');
	if (pos != std::string::npos)
	    return name.substr(0, pos);
	return std::string();
    }
}

dqm_config::ConditionsManager::ConditionsManager(const std::string & partition_name)
    : m_partition(partition_name)
{
    ;
}

void
dqm_config::ConditionsManager::addCondition(const std::string& name,
    const std::string& source, const std::string& default_value)
{
    std::unique_lock < std::mutex > lock(m_mutex);

    Conditions::iterator it = m_conditions.find(name);
    if (it != m_conditions.end())
    {
	return ;
    }

    std::string info_name = get_info_name(source);
    it = m_conditions_sources.find(info_name);
    if (it == m_conditions_sources.end())
    {
	it = m_conditions_sources.insert(
	    std::make_pair(info_name, std::shared_ptr<ConditionSource>(
		    new ConditionSource(m_partition, info_name)))).first;
    }

    it->second->addCondition(name, get_attribute_name(source), default_value);

    m_conditions.insert(std::make_pair(name, it->second));
}

bool
dqm_config::ConditionsManager::getConditionValue(const std::string& name,
    std::string& value) const
{
    std::unique_lock lock(m_mutex);
    Conditions::const_iterator it = m_conditions.find(name);
    if (it == m_conditions.end())
    {
	return it->second->getConditionValue(name, value);
    }
    return false;
}

bool
dqm_config::ConditionsManager::getConditionValue(const std::string& name,
    double& value) const
{
    std::unique_lock < std::mutex > lock(m_mutex);
    Conditions::const_iterator it = m_conditions.find(name);
    if (it == m_conditions.end())
    {
	return it->second->getConditionValue(name, value);
    }
    return false;
}

