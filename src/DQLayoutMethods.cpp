/* DQLayoutMethods.cpp
   Author Serguei Kolos 31/01/2007
   Implements methods of the DQLayout database class
*/
#include <functional>

#include <dqm_config/dal/DQLayout.h>

#include <boost/config/warning_disable.hpp>
#include <boost/phoenix/operator.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/fusion/adapted/std_pair.hpp>

namespace client
{
    namespace qi = boost::spirit::qi;
    namespace ascii = boost::spirit::ascii;
    namespace phx = boost::phoenix;

    typedef
    boost::spirit::context<
        boost::fusion::cons<std::string&, boost::fusion::nil>,
        boost::fusion::vector1<std::string>
    > f_context;

    void repeat(int const & num, const f_context & con)
    {
	std::string & dst = boost::fusion::at_c<0>(con.attributes);
	const std::string & tok = boost::fusion::at_c<0>(con.locals);
        for(int i = 0; i < num; ++i) {
	    dst += tok;
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    //  The cells occupation grammar
    ///////////////////////////////////////////////////////////////////////////
    template <typename Iterator>
    struct children_grid : qi::grammar<Iterator, std::string(), ascii::space_type, qi::locals<std::string>>
    {
	children_grid() : children_grid::base_type(term)
        {
            using qi::uint_;
            using qi::_1;
            using qi::_val;

            term = *(
            	 +ascii::char_("01") [_val += _1]
                | ascii::char_('(') >> term [qi::_a = _1] >> ascii::char_(')') >>
                  ((ascii::char_('{') >> uint_ [&repeat] >> ascii::char_('}')) | qi::attr(1)[&repeat])
            );
        }

        qi::rule<Iterator, std::string(), ascii::space_type, qi::locals<std::string>> term;
    };

    ///////////////////////////////////////////////////////////////////////////
    //  The cells spacing grammar
    ///////////////////////////////////////////////////////////////////////////
    template <typename Iterator>
    struct spacing
      : qi::grammar<Iterator, std::map<unsigned int, double>(), ascii::space_type>
    {
	  spacing()
          : spacing::base_type(query)
        {
            using qi::uint_;
            using qi::double_;

            query =  *pair;
            pair  =  uint_ >> ':' >> double_;
        }
        qi::rule<Iterator, std::map <unsigned int, double>(), ascii::space_type> query;
        qi::rule<Iterator, std::pair<unsigned int, double>(), ascii::space_type> pair;
    };

    ///////////////////////////////////////////////////////////////////////////
    //  The cells span grammar
    ///////////////////////////////////////////////////////////////////////////
    template <typename Iterator>
    struct span
    : qi::grammar<Iterator, std::map<unsigned int, std::pair<unsigned int, unsigned int>>(), ascii::space_type>
    {
	span()
	: span::base_type(query)
      {
	  using qi::uint_;

	  query =  *token;
	  token  =  uint_ >> ':' >> pair;
	  pair = uint_ >> 'x' >> uint_;
      }
      qi::rule<Iterator, std::map <unsigned int, std::pair<unsigned int, unsigned int>>(), ascii::space_type> query;
      qi::rule<Iterator, std::pair<unsigned int, std::pair<unsigned int, unsigned int>>(), ascii::space_type> token;
      qi::rule<Iterator, std::pair<unsigned int, unsigned int>(), ascii::space_type> pair;
    };

    ///////////////////////////////////////////////////////////////////////////
    //  The row/column headers grammar
    ///////////////////////////////////////////////////////////////////////////
    template <typename Iterator>
    struct header
    : qi::grammar<Iterator, std::pair<std::pair<unsigned int, unsigned int>, std::string>()>
    {
	header(unsigned int cnt)
	: header::base_type(query),
	  m_cnt(cnt)
      {
	  using qi::uint_;
	  using qi::char_;
	  using qi::attr;

	  query = pair >> +char_;
	  pair  = count >> span;
	  count = (uint_ >> ':') | attr(m_cnt);
	  span  = (uint_ >> ':') | attr(1);
      }
      qi::rule<Iterator, std::pair<std::pair<unsigned int, unsigned int>, std::string>()> query;
      qi::rule<Iterator, std::pair<unsigned int, unsigned int>()> pair;
      qi::rule<Iterator, unsigned int()> span;
      qi::rule<Iterator, unsigned int()> count;

      const unsigned int m_cnt;
    };

    ///////////////////////////////////////////////////////////////////////////
    //  The labels grammar
    ///////////////////////////////////////////////////////////////////////////
    template<typename Iterator>
    struct labels
    : qi::grammar<Iterator,
	std::pair<
	    std::pair<std::pair<unsigned int, unsigned int>,
	        std::pair<unsigned int, unsigned int>>, std::string>()>
    {
	labels()
	    : labels::base_type(query)
	{
	    using qi::uint_;
	    using qi::char_;
	    using qi::attr;

	    query = position >> ':' >> +char_;
	    position = row_col >> span;
	    row_col = uint_ >> ' ' >> uint_;
	    span = (' ' >> uint_ >> 'x' >> uint_) | attr(std::make_pair(1, 1));
	}
	qi::rule<Iterator,
	    std::pair<
	        std::pair<std::pair<unsigned int, unsigned int>,std::pair<unsigned int, unsigned int>>,
	        std::string>()> query;
	qi::rule<Iterator,
	    std::pair<std::pair<unsigned int, unsigned int>,std::pair<unsigned int, unsigned int>>()> position;
	qi::rule<Iterator, std::pair<unsigned int, unsigned int>()> row_col;
	qi::rule<Iterator, std::pair<unsigned int, unsigned int>()> span;
    };
}

std::string
dqm_config::dal::DQLayout::get_children_grid() const
{
    typedef std::string::const_iterator iterator_type;
    typedef client::children_grid<iterator_type> grid;

    const std::string & pattern = get_ChildrenGrid();
    std::string::const_iterator iter = pattern.begin();
    std::string::const_iterator end = pattern.end();
    std::string result;
    bool r = phrase_parse(iter, end, grid(), client::ascii::space, result);

    if (r && iter == end) {
        return result.empty() ? std::string("1") : result;
    }
    else {
	std::string rest(iter, end);
	ERS_LOG("Bad value for the ChildrenGrid parameter of the '" << UID()
	    << "' layout is given, parsing stopped at '" << rest << "'" );
	return std::string("1");
    }
}

std::map<unsigned int, double>
dqm_config::dal::DQLayout::get_row_spacing() const
{
    typedef std::string::const_iterator iterator_type;
    typedef client::spacing<iterator_type> spacing;

    const std::string & pattern = get_RowSpacing();
    std::string::const_iterator iter = pattern.begin();
    std::string::const_iterator end = pattern.end();
    std::map<unsigned int, double> result;
    bool r = phrase_parse(iter, end, spacing(), client::ascii::space, result);

    if (!r || iter != end) {
	std::string rest(iter, end);
	ERS_LOG("Bad value for the RowSpacing parameter of the '" << UID()
	    << "' layout is given, parsing stopped at '" << rest << "'" );
    }

    return result;
}

std::map<unsigned int, double>
dqm_config::dal::DQLayout::get_column_spacing() const
{
    typedef std::string::const_iterator iterator_type;
    typedef client::spacing<iterator_type> spacing;

    const std::string & pattern = get_ColumnSpacing();
    std::string::const_iterator iter = pattern.begin();
    std::string::const_iterator end = pattern.end();
    std::map<unsigned int, double> result;
    bool r = phrase_parse(iter, end, spacing(), client::ascii::space, result);

    if (!r || iter != end) {
	std::string rest(iter, end);
	ERS_LOG("Bad value for the RowSpacing parameter of the '" << UID()
	    << "' layout is given, parsing stopped at '" << rest << "'" );
    }

    return result;
}

std::map<unsigned int, double>
dqm_config::dal::DQLayout::get_row_shifts() const
{
    typedef std::string::const_iterator iterator_type;
    typedef client::spacing<iterator_type> spacing;

    const std::string & pattern = get_RowShifts();
    std::string::const_iterator iter = pattern.begin();
    std::string::const_iterator end = pattern.end();
    std::map<unsigned int, double> result;
    bool r = phrase_parse(iter, end, spacing(), client::ascii::space, result);

    if (!r || iter != end) {
	std::string rest(iter, end);
	ERS_LOG("Bad value for the RowShifts parameter of the '" << UID()
	    << "' layout is given, parsing stopped at '" << rest << "'" );
    }

    return result;
}

std::map<unsigned int, double>
dqm_config::dal::DQLayout::get_column_shifts() const
{
    typedef std::string::const_iterator iterator_type;
    typedef client::spacing<iterator_type> spacing;

    const std::string & pattern = get_ColumnShifts();
    std::string::const_iterator iter = pattern.begin();
    std::string::const_iterator end = pattern.end();
    std::map<unsigned int, double> result;
    bool r = phrase_parse(iter, end, spacing(), client::ascii::space, result);

    if (!r || iter != end) {
	std::string rest(iter, end);
	ERS_LOG("Bad value for the RowShifts parameter of the '" << UID()
	    << "' layout is given, parsing stopped at '" << rest << "'" );
    }

    return result;
}

std::map<unsigned int, std::pair<unsigned int, unsigned int>>
dqm_config::dal::DQLayout::get_children_span() const
{
    typedef std::string::const_iterator iterator_type;
    typedef client::span<iterator_type> span;

    const std::string & pattern = get_ChildrenSpan();
    std::string::const_iterator iter = pattern.begin();
    std::string::const_iterator end = pattern.end();
    std::map<unsigned int, std::pair<unsigned int, unsigned int>> result;
    bool r = phrase_parse(iter, end, span(), client::ascii::space, result);

    if (!r || iter != end) {
	std::string rest(iter, end);
	ERS_LOG("Bad value for the ChildrenSpan parameter of the '" << UID()
	    << "' layout is given, parsing stopped at '" << rest << "'" );
    }

    return result;
}

std::map<std::pair<unsigned int, unsigned int>, std::string>
dqm_config::dal::DQLayout::get_row_headers() const
{
    typedef std::string::const_iterator iterator_type;
    typedef client::header<iterator_type> header;

    const std::vector<std::string> & pattern = get_RowHeaders();
    std::map<std::pair<unsigned int, unsigned int>, std::string> result;

    for (unsigned int i = 0; i < pattern.size(); ++i ) {

	std::string::const_iterator iter = pattern[i].begin();
	std::string::const_iterator end = pattern[i].end();
	std::pair<std::pair<unsigned int, unsigned int>, std::string> pair;
	bool r = parse(iter, end, header(i), pair);

	if (!r || iter != end) {
	    ERS_LOG("Bad value for the RowHeaders parameter of the '" << UID()
		<< "' layout is given, parsing failed fo '" << pattern[i] << "' item" );
	}
	else {
	    result.insert(pair);
	}
    }
    return result;
}

std::map<std::pair<unsigned int, unsigned int>, std::string>
dqm_config::dal::DQLayout::get_column_headers() const
{
    typedef std::string::const_iterator iterator_type;
    typedef client::header<iterator_type> header;

    const std::vector<std::string> & pattern = get_ColumnHeaders();
    std::map<std::pair<unsigned int, unsigned int>, std::string> result;

    for (unsigned int i = 0; i < pattern.size(); ++i ) {

	std::string::const_iterator iter = pattern[i].begin();
	std::string::const_iterator end = pattern[i].end();
	std::pair<std::pair<unsigned int, unsigned int>, std::string> pair;
	bool r = parse(iter, end, header(i), pair);

	if (!r || iter != end) {
	    ERS_LOG("Bad value for the ColumnHeaders parameter of the '" << UID()
		<< "' layout is given, parsing failed fo '" << pattern[i] << "' item" );
	}
	else {
	    result.insert(pair);
	}
    }
    return result;
}

std::map<
    std::pair<std::pair<unsigned int, unsigned int>, std::pair<unsigned int, unsigned int>>,
    std::string>
dqm_config::dal::DQLayout::get_labels() const
{
    typedef std::string::const_iterator iterator_type;
    typedef client::labels<iterator_type> labels;

    const std::vector<std::string> & pattern = get_Labels();
    std::map<
	std::pair<std::pair<unsigned int, unsigned int>,
	    std::pair<unsigned int, unsigned int>>, std::string> result;

    for (unsigned int i = 0; i < pattern.size(); ++i)
    {
	std::string::const_iterator iter = pattern[i].begin();
	std::string::const_iterator end = pattern[i].end();
	std::pair<
	    std::pair<std::pair<unsigned int, unsigned int>,
	        std::pair<unsigned int, unsigned int>>, std::string> pair;
	bool r = parse(iter, end, labels(), pair);

	if (!r || iter != end)
	{
	    ERS_LOG("Bad value for the Labels parameter of the '" << UID()
		<< "' region is given, parsing failed for '" << pattern[i] << "' item");
	}
	else
	{
	    result.insert(pair);
	}
    }
    return result;
}
