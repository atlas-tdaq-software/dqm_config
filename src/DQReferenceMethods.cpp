/* DQReferenceMethods.cpp
 Author Serguei Kolos 31/01/2007
 Implements methods of the DQReference database class
 */

#include <oh/OHUtil.h>

#include <dqm_config/AlgorithmConfig.h>
#include <dqm_config/dal/DQReference.h>

std::string
dqm_config::dal::DQReference::get_is_name(const std::string & default_object_name) const
{
    std::string source = get_Source();
    if (source.find_first_of("is:") == 0)
    {
	ERS_DEBUG(3, "Reference is pointing to IS");
	return source.substr(3);
    }
    else if (source.find_first_of("file:") == 0)
    {
	std::string file;
	std::string name = default_object_name;
	get_file_and_name(file, name);
	std::replace(file.begin(), file.end(), '.', '_');

	return dqm_config::AlgorithmConfig::ISServerName + "." + file + "." + name;
    }
    else
    {
	throw dqm_config::BadConfig(ERS_HERE, UID(),
	    "Bad reference format '" + source + "'");
    }
}

std::string
dqm_config::dal::DQReference::get_source(const std::string & default_object_name) const
{
    std::string source = get_Source();
    if (source.find_first_of("is:") == 0)
    {
	ERS_DEBUG(3, "Reference is pointing to IS");
	return source.substr(3);
    }

    if (source.find_first_of("file:") == 0)
    {
	std::string file;
	std::string name = default_object_name;
	get_file_and_name(file, name);

	return file + ":" + name;
    }
    return source;
}

bool
dqm_config::dal::DQReference::requires_publication() const
{
    return get_Source().find_first_of("is:");
}

void
dqm_config::dal::DQReference::get_file_and_name(
    std::string & file_name, std::string & object_name) const
{
    std::string source = get_Source();

    if (source.find_first_of("file:") == 0)
    {
	std::string file = source.substr(5);
	std::string::size_type pos = file.find_first_of(':');

	file_name = file.substr(0, pos);

	if (pos != std::string::npos)
	    object_name = file.substr(pos+1);
    }
}
