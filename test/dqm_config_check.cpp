/* dqm_config_check.cpp
 Author Serguei Kolos 07/08/16
 */
#include <cmdl/cmdargs.h>

#include <iostream>
#include <memory>
#include <string>
#include <vector>
#include <unordered_map>

#include <dqm_config/dal/DQTemplateParameter.h>
#include <dqm_config/dal/DQParameter.h>
#include <dqm_config/dal/DQRegion.h>
#include <dqm_config/dal/DQTemplateRegion.h>

#include <ipc/core.h>
#include <dal/util.h>
#include <dal/Partition.h>

struct Node
{
    std::string path;
    std::string templateId;
    bool isRegion;

    Node()
    {
        isRegion = true;
    }

    Node(const std::string & p, bool is_region)
    {
        path = p;
        isRegion = is_region;
    }

    Node(const std::string & tid, const std::string & p, bool is_region)
    {
        path = p;
        templateId = tid;
        isRegion = is_region;
    }
};

std::unordered_map<std::string, std::vector<Node>> duplicates;
std::unordered_map<std::string, Node> instances;

void checkForDuplicates(const std::string & id, const Node & node)
{
    std::unordered_map<std::string, Node>::iterator it = instances.find(id);
    if (it != instances.end()) {
        std::vector<Node> & nn = duplicates[id];
        if (nn.empty()) {
            nn.push_back(it->second);
        }
        nn.push_back(node);
    }
    else {
        instances[id] = node;
    }
}

struct ConsistencyCheck
{
    static bool visitor(const dqm_config::dal::DQRegion * region, const std::string & variables="", const std::string & path="")
    {
        const dqm_config::dal::DQTemplateRegion * tmpl = region->cast<
                dqm_config::dal::DQTemplateRegion>();
        if (!tmpl) {
            ERS_DEBUG(3, "Processing '" << region->UID() << "' DQ Region");
            checkForDuplicates(region->UID(), Node(path, true));
            const std::vector<const dqm_config::dal::DQRegion*> & children = region->get_DQRegions();
            for (size_t j = 0; j < children.size(); j++) {
		visitor(children[j], variables + "," + region->m_tokens, path + region->UID() + " -> ");
            }
        }
        else {
            ERS_DEBUG(3,
                    "Processing '" << region->UID() << "' DQ Template Region");

            const std::vector<const dqm_config::dal::DQRegion *> & rr =
                    tmpl->get_instances(variables);
            for (size_t i = 0; i < rr.size(); i++) {
                checkForDuplicates(rr[i]->UID(), Node(region->UID(), path, true));

                const std::vector<const dqm_config::dal::DQRegion*> & children =
                        rr[i]->get_DQRegions();
                for (size_t j = 0; j < children.size(); j++) {
                    visitor(children[j], variables + "," + rr[i]->m_tokens, path + rr[i]->UID() + " -> ");
                }
            }
            return true;
        }

        const std::vector<const dqm_config::dal::DQParameter *> & params =
                region->get_DQParameters();
        for (size_t i = 0; i < params.size(); i++) {
            const dqm_config::dal::DQTemplateParameter * tmpl = params[i]->cast<
                    dqm_config::dal::DQTemplateParameter>();
            if (!tmpl) {
                ERS_DEBUG(3,
                        "Processing '" << params[i]->UID() << "' DQ Parameter");
                checkForDuplicates(params[i]->UID(), Node(path, false));
            }
            else {
                ERS_DEBUG(3,
                        "Processing '" << params[i]->UID() << "' DQ Template Parameter");
                const std::vector<const dqm_config::dal::DQParameter *> & pp =
                        tmpl->get_instances(variables + "," + region->m_tokens);
                for (size_t j = 0; j < pp.size(); j++) {
                    checkForDuplicates(pp[j]->UID(),
                            Node(params[i]->UID(), path, false));
                }
            }
        }

        return true;

    }
};

int main(int argc, char ** argv)
{
    IPCCore::init(argc, argv);

    CmdArgStr database('d', "database", "database-name",
            "DQM Configuration database to be used.", CmdArg::isREQ);
    CmdArgStr partition('p', "partition", "partition-name",
            "Partition to work in.", CmdArg::isREQ);

    // Declare command object and its argument-iterator
    CmdLine cmd(*argv, &database, &partition, NULL);
    CmdArgvIter arg_iter(--argc, ++argv);
    partition = "";
    // Parse arguments
    cmd.parse(arg_iter);

    try {
        Configuration * config = new Configuration((const char *) database);
        const daq::core::Partition * part_config = config->get<
                daq::core::Partition>((const char*) partition);
        if (!part_config) {
            std::cerr << "Partition '" << (const char*) partition
                    << "' is not found in the database" << std::endl;
            return 1;
        }
        config->register_converter(
                new daq::core::SubstituteVariables(*part_config));

        std::vector<const dqm_config::dal::DQRegion *> regions;
        dqm_config::dal::DQRegion::get_root_regions(*config, *part_config,
                regions);

        for (size_t i = 0; i < regions.size(); i++) {
            ConsistencyCheck::visitor(regions[i]);
        }
    }
    catch (ers::Issue & ex) {
        ers::fatal(ex);
        return 1;
    }

    if (duplicates.empty()) {
        return 0;
    }

    std::unordered_map<std::string, std::vector<Node>>::iterator it =
            duplicates.begin();

    for (; it != duplicates.end(); ++it) {
        std::cout << "There are multiple objects using '\033[31m" << it->first
                << "\033[0m' id:" << std::endl;
        for (size_t i = 0; i < it->second.size(); ++i) {
            Node n = it->second[i];
            if (!n.templateId.empty()) {
                std::cout << "\t" << (n.isRegion ? "the region" : "the parameter")
                        << " instantiated from '\033[1m" << n.path << n.templateId
                        << "\033[0m' template" << std::endl;
            }
            else {
                std::cout << "\t" << (n.isRegion ? "plain region '\033[1m" : "plain parameter '\033[1m")
                        << n.path << it->first << "\033[0m'" << std::endl;
            }
        }
    }

    return 1;
}
