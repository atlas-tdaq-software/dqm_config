/* dqm_config_test.cpp
 Author Serguei Kolos 15/09/15
 */
#include <cmdl/cmdargs.h>

#include <memory>
#include <string>
#include <vector>
#include <unordered_map>

#include <dqm_core/Region.h>
#include <dqm_core/ParameterConfig.h>
#include <dqm_core/LibraryManager.h>
#include <dqm_core/IOManager.h>

#include <dqm_config/AlgorithmConfig.h>
#include <dqm_config/dal/DQAgent.h>
#include <dqm_config/dal/DQAlgorithm.h>
#include <dqm_config/dal/DQInput.h>
#include <dqm_config/dal/DQOutput.h>
#include <dqm_config/dal/DQParameter.h>
#include <dqm_config/dal/DQRegion.h>
#include <dqm_config/dal/DQSummaryMaker.h>

#include <ipc/core.h>
#include <dal/util.h>
#include <dal/Partition.h>

std::string partition_name;

struct Parameter
{
    Parameter(Configuration * configuration, dqm_config::AlgorithmConfigFactory * acf,
	dqm_core::Input * in, dqm_core::Output * out,
	std::vector<boost::shared_ptr<dqm_core::Region>> & r)
	: config(configuration),
	  ac_factory(acf),
	  input(in),
	  output(out),
	  regions(r)
    {
	;
    }

    Configuration * config;
    dqm_config::AlgorithmConfigFactory * ac_factory;
    boost::shared_ptr<dqm_core::Region> core_region;
    dqm_core::Input * input;
    dqm_core::Output * output;
    std::vector<boost::shared_ptr<dqm_core::Region>> & regions;
};

bool
visitor(const dqm_config::dal::DQRegion * region, Parameter & parameter)
{
    try
    {
	dqm_core::LibraryManager::instance().loadLibrary(
		region->get_DQSummaryMaker()->get_LibraryName());
	dqm_core::RegionConfig rconfig(region->get_DQSummaryMaker()->UID(),
	    region->get_Weight());
	if (!parameter.core_region)
	{
	    ERS_DEBUG(1, "Creating '" << region->UID() << "' root region");
	    parameter.core_region = dqm_core::Region::createRootRegion(region->UID(),
		*parameter.input, *parameter.output, rconfig);
	    parameter.regions.push_back(parameter.core_region);
	}
	else
	{
	    if (region->is_referenced_by_agent())
	    {
		// this region is already handled by another agent
		// we have to create a proxy region here
		ERS_DEBUG(1, "Creating '" << region->UID() << "' remote region");
		parameter.core_region->addRemoteRegion(region->UID(), rconfig);
		return false;
	    }
	    else
	    {
		ERS_DEBUG(1,
		    "Creating '" << region->UID() << "' sub region of the '"
		        << parameter.core_region->getName() << "' region");
		parameter.core_region = parameter.core_region->addRegion(region->UID(),
		    rconfig);
	    }
	}
    }
    catch (ers::Issue & ex)
    {
	ers::error(ex);
	return false;
    }

    std::vector<const dqm_config::dal::DQParameter *> parameters;
    region->get_all_parameters(parameters);
    for (size_t i = 0; i < parameters.size(); i++)
    {
	try
	{
	    std::shared_ptr<dqm_config::AlgorithmConfig> algo_config =
		parameter.ac_factory->createAlgorithmConfig(*parameters[i]);

	    dqm_core::LibraryManager::instance().loadLibrary(
		    parameters[i]->get_DQAlgorithm()->get_LibraryName());

	    const std::vector<std::string> & input = parameters[i]->get_InputDataSource();
	    dqm_core::ParameterConfig pconfig(
		input,					// input data
		parameters[i]->get_DQAlgorithm()->UID(),// algo name
		parameters[i]->get_Weight(),		// severity level for that parameter
		algo_config);				// configuration of the algo
	    parameter.core_region->addParameter(parameters[i]->UID(), pconfig);
	}
	catch (ers::Issue & ex)
	{
	    ers::error(ex);
	}

    }

    return true;

}

int main(int argc, char ** argv)
{
    IPCCore::init(argc, argv);

    CmdArgStr database('d', "database", "database-name",
	"DQM Configuration database to be used.", CmdArg::isREQ);
    CmdArgStr partition('p', "partition", "partition-name", "Partition to work in.", CmdArg::isREQ);
    CmdArgStr agent('a', "agent", "agent-name", "DQ Agent name.", CmdArg::isREQ);
    CmdArgBool use_dummy_streams('D', "use-dummy-streams", "use dummy streams which never fail.");

    // Declare command object and its argument-iterator
    CmdLine cmd(*argv, &database, &partition, &agent, &use_dummy_streams, NULL);
    CmdArgvIter arg_iter(--argc, ++argv);
    partition = "";
    // Parse arguments
    cmd.parse(arg_iter);

    //load DQM database
    Configuration * config;
    partition_name = (const char*) partition;
    std::string agent_name = (const char*) agent;

    try
    {
	config = new Configuration((const char *) database);
    }
    catch (daq::config::Generic & ex)
    {
	ers::fatal(ex);
	return 1;
    }

    try
    {
        const daq::core::Partition * part_config = config->get < daq::core::Partition
            > ((const char *) partition);
        if (!part_config)
        {
            std::cerr << "Partition '" << (const char *) partition
                << "' is not found in the database" << std::endl;
            return 1;
        }
        config->register_converter(
            new daq::core::SubstituteVariables(*part_config));
    }
    catch (daq::config::Generic & ex)
    {
        ers::fatal(ex);
        return 1;
    }

    const dqm_config::dal::DQAgent * dq_agent = config->get<dqm_config::dal::DQAgent>(agent_name);

    if (!dq_agent)
    {
	std::cerr << "DQ Agent '" << agent_name
	    << "' is not found in the database" << std::endl;
	return 1;
    }

    std::unique_ptr<dqm_core::Input> input;
    std::unique_ptr<dqm_core::Output> output;

    if (!use_dummy_streams)
    {
	const std::vector<const dqm_config::dal::DQInput *> & inputs =
	    dq_agent->get_DQInputs();
	for (size_t i = 0; i < inputs.size(); ++i)
	{
	    try
	    {
		dqm_core::LibraryManager::instance().loadLibrary(inputs[i]->get_Library());
		input.reset(
		    dqm_core::IOManager::instance().createInput(inputs[i]->get_Name(),
			inputs[i]->get_Parameters()));
	    }
	    catch (ers::Issue & ex)
	    {
		ers::error(ex);
	    }
	}

	const std::vector<const dqm_config::dal::DQOutput *> & outputs =
	    dq_agent->get_DQOutputs();
	for (size_t i = 0; i < outputs.size(); ++i)
	{
	    try
	    {
		dqm_core::LibraryManager::instance().loadLibrary(outputs[i]->get_Library());
		output.reset(
		    dqm_core::IOManager::instance().createOutput(outputs[i]->get_Name(),
			outputs[i]->get_Parameters()));
	    }
	    catch (ers::Issue & ex)
	    {
		ers::error(ex);
	    }
	}
    }

    if (!input)
    {
	try {
	    dqm_core::LibraryManager::instance().loadLibrary( "libdqm_dummy_io.so" );
	    input.reset( dqm_core::IOManager::instance().createInput(
		"DummyInput", std::vector<std::string>() ) );
	}
	catch( dqm_core::Exception & ex ) {
	    ers::fatal( ex );
	    return 1;
	}
    }

    if (!output)
    {
	try {
	    dqm_core::LibraryManager::instance().loadLibrary( "libdqm_dummy_io.so" );
	    output.reset( dqm_core::IOManager::instance().createOutput(
		"DummyOutput", std::vector<std::string>() ) );
	}
	catch( dqm_core::Exception & ex ) {
	    ers::fatal( ex );
	    return 1;
	}
    }

    std::vector<boost::shared_ptr<dqm_core::Region>> top_regions;
    dqm_config::AlgorithmConfigFactory ah(partition_name);

    Parameter parameter(config, &ah, input.get(), output.get(), top_regions);
    dq_agent->visit_all_regions(std::bind(&::visitor, std::placeholders::_1, parameter));

    for (size_t i = 0; i < top_regions.size(); i++)
    {
	top_regions[i]->print(std::cout);
    }

    std::cout << std::endl;

    std::cout << "References used in the configuration:" << std::endl;
    ah.getReferenceManager().print(std::cout);
    std::cout << std::endl;

    return 0;
}
