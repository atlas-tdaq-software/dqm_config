/* dqm_config_update.cpp
 Author Serguei Kolos 15/01/14
 */

#include <boost/algorithm/string.hpp>

#include <cmdl/cmdargs.h>

#include <dqm_config/exceptions.h>
#include <dqm_config/AlgorithmConfig.h>
#include <dqm_config/is/DQParameter.h>

#include <ipc/core.h>
#include <is/infodictionary.h>

void parse_input(const std::string & name, CmdArgStrList & source,
    std::vector<std::string> & dest)
{
    for( unsigned int i = 0; i < source.count(); ++i )
    {
	std::vector<std::string> key_val;
	std::string token((const char*)source[i]);
	boost::split(key_val, token, boost::is_any_of("="));
	if (key_val.size() == 2)
	    dest.push_back(token);
	else
	    throw dqm_config::BadConfig(ERS_HERE, name, "'" + token + "' value has invalid format");
    }
}

int main(int argc, char ** argv)
{
    IPCCore::init(argc, argv);

    CmdArgStr parameter_name('d', "dq-parameter", "parameter-name",
		    "DQ Parameter which has to be reconfigured.", CmdArg::isREQ);

    CmdArgStr partition_name('p', "partition", "partition-name",
		    "TDAQ Partition to be used.", CmdArg::isREQ);

    CmdArgStrList parameters('a', "algorithm-parameters", "algorithm-parameters-config",
		    "algorithm's parameters to be modified. "
		    "They have to be given as 'name=value' tokens. For example "
		    "'-a minstat=10 xmin=0 xmax=100", CmdArg::isLIST);

    CmdArgStrList red_thresholds('r', "algorithm-red-thresholds", "algorithm-thresholds-config",
		    "algorithm's red thresholds to be modified. "
		    "They have to be given as 'name=value' tokens. For example "
		    "'-r t0_lowBound=10, t0_highBound=100", CmdArg::isLIST);

    CmdArgStrList green_thresholds('g', "algorithm-green-thresholds", "algorithm-thresholds-config",
		    "algorithm's green thresholds to be modified. "
		    "They have to be given as 'name=value' tokens. For example "
		    "-g t0_lowBound=5, t0_highBound=9", CmdArg::isLIST);

    // Declare command object and its argument-iterator
    CmdLine cmd(*argv, &parameter_name, &partition_name, &red_thresholds, &green_thresholds, NULL);
    CmdArgvIter arg_iter(--argc, ++argv);

    // Parse arguments
    cmd.parse(arg_iter);

    IPCPartition partition((const char*)partition_name);
    ISInfoDictionary id(partition);

    dqm_config::is::DQParameter info;
    std::string name((const char*)parameter_name);
    try
    {
	parse_input(name, parameters, info.Parameters);
	parse_input(name, red_thresholds, info.RedThresholds);
	parse_input(name, green_thresholds, info.GreenThresholds);
	id.checkin(dqm_config::AlgorithmConfig::ISServerName + "." + name, info);
    }
    catch (ers::Issue & ex)
    {
	ers::fatal(ex);
	return 1;
    }

    return 0;
}
